# RPG Character

## Description
RPG Character is a console application in C# which allows you to create several RPG characters with certain attributes.

## RPG Character
The RPG character is initiated with a:
- Name
- Type (Warrior, Rogue, Ranger, Mage)
- level (default lvl 1)
- Base primary attributes (strength, dexterity, intelligence) based on the character type
- Damage

The RPG character has 4 equipment slots (head, body, legs, weapon) the first three can be used for equipping armor and the last can be used to equip a weapon of choice.

## Weapon
A user can create a weapon which can later be equipped to a character.
A weapon is initiated with a:
- Name
- Required level
- Weapon type
- Slot (default: weapon-slot)
- WeaponAttributes (damage, attack speed)

## Armor
A user can create an armor object which can later be equipped to a character.
An armor object is initiated with a:
- Name
- Required level
- Armor type
- Slot
- Primary attributes (strength, dexterity, intelligence)

## Visuals
![Character Stats](Images/characterStats.jpg?raw=true "Character Stats")

## Installation
Use cmd to clone project.

SSH:
```bash
git clone git@gitlab.com:LisettedeWilde/assignment-1-rpg-character.git
```
HTTPS:
```bash
git clone https://gitlab.com/LisettedeWilde/assignment-1-rpg-character.git
```

Open the RPG Characters.sln file in Visual Studio and run the project.

## Usage
A few testcases have been added in the program.cs file.

```cs
// creates Mage Object with the name Merlin
Mage mage = new Mage("Merlin");

// creates Weapon Object with:
// name: Indestructable Staff 
// level: 1
// type: Staff
// damage: 6
// attackspeed: 4
Weapon staff = new Weapon("Indestructable taff", 1, WeaponTypes.STAFF, new WeaponAttributes(6, 4));

// adds the Weapon Object to the Mage Object
mage.EquipWeapon(staff);

// creates Armor Object with:
// name: Common Cloth 
// level: 1 
// it can be added to the equipmentslot Body
// type: Cloth 
// primary attributes: strength: 1, dexterity: 2, intelligence: 3
Armor cloth = new("Common Cloth", 1, Slots.BODY, ArmorTypes.CLOTH, new PrimaryAttributes(1, 2, 3));

// adds the Armor Object to the Mage Object
mage.EquipArmor(cloth);

// increases the Mage Object level by 1
mage.GainLevel();

// returns the characters stats to the console
mage.ReturnCharacterStats();
```
