﻿using RPG_Characters;
using RPG_Characters.Attributes;
using RPG_Characters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPG_Characters.Exceptions;
using static RPG_Characters.Equipment.ItemSlots;
using static RPG_Characters.Equipment.Armor;
using System.IO;

namespace RPG_CharacterTests.EquipmentTests
{
    public class TestEquipment
    {
        /// <summary>
        /// Tests if an InvalidWeaponException is thrown when a level one character equips a level two weapon
        /// </summary>
        [Fact]
        public void EquipWeapon_WarriorLevelOneAxeLevelTwo_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Weapon testAxe = new Weapon("Common Axe", 2, Weapon.WeaponTypes.AXE, new WeaponAttributes(7, 1.1));

            // Act

            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }

        /// <summary>
        /// Tests if an InvalidWeaponException is thrown when a character type equips an invalid weapon
        /// </summary>
        [Fact]
        public void EquipWeapon_WarriorEquipsBow_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Weapon testBow = new Weapon("Common Bow", 1, Weapon.WeaponTypes.BOW, new WeaponAttributes(12, 0.8));

            // Act

            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }

        /// <summary>
        /// Tests if an InvalidArmorException is thrown when a level one character equips level two armor
        /// </summary>
        [Fact]
        public void EquipArmor_WarriorLevelOneArmorLevelTwo_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Armor testPlateBody = new Armor("Common place body armor", 2, Slots.BODY, ArmorTypes.PLATE, new PrimaryAttributes(1, 0, 0));

            // Act

            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlateBody));
        }

        /// <summary>
        /// Tests if an InvalidArmorException is thrown when a character equips an invalid weapon
        /// </summary>
        [Fact]
        public void EquipArmor_WarriorEquipsCloth_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Armor testClothHead = new Armor("Common cloth head armor", 1, Slots.HEAD, ArmorTypes.CLOTH, new PrimaryAttributes(0, 0, 5));

            // Act

            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testClothHead));
        }

        /// <summary>
        /// Tests whether a confirmation message is sent when a weapon is equipped
        /// </summary>
        [Fact]
        public void AddWeapon_WarriorEquipdAxe_ShouldShowConfirmationMessage()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Weapon testAxe = new Weapon("Common Axe", 1, Weapon.WeaponTypes.AXE, new WeaponAttributes(7, 1.1));
            string expected = "New weapon equipped!\r\n";
            var stringWriter = new StringWriter();

            // Act
            Console.SetOut(stringWriter);
            warrior.EquipWeapon(testAxe);

            // Assert
            var actual = stringWriter.ToString();
            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Tests whether a confirmation message is sent when armor is equipped
        /// </summary>
        [Fact]
        public void AddArmor_WarriorEquipdPlate_ShouldShowConfirmationMessage()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Armor testPlateBody = new Armor("Common place body armor", 1, Slots.BODY, ArmorTypes.PLATE, new PrimaryAttributes(1, 0, 0));
            string expected = "New armor equipped!\r\n";
            var stringWriter = new StringWriter();

            // Act
            Console.SetOut(stringWriter);
            warrior.EquipArmor(testPlateBody);

            // Assert
            var actual = stringWriter.ToString();
            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Tests if the damage is correctly calculated for a level one character without a weapon or armor
        /// </summary>
        [Fact]
        public void CalculateDamage_LevelOneWarriorNoArmorNoWeapon_ShouldHaveCertainDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            double expected = 1 * (1 + 5 * 0.01);

            // Act
            double actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests if the damage is correctly calculated for a level one character with a weapon equipped
        /// </summary>
        [Fact]
        public void CalculateDamage_LevelOneWarriorWeaponEquipped_ShouldHaveCertainDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Weapon testAxe = new Weapon("Common Axe", 1, Weapon.WeaponTypes.AXE, new WeaponAttributes(7, 1.1));
            double expected = (7 * 1.1) * (1 + 5 * 0.01);

            // Act
            warrior.EquipWeapon(testAxe);
            double actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests if the damage is correctly calculated for a level one character with a weapon and armor equipped
        /// </summary>
        [Fact]
        public void CalculateDamge_LevelOneWarriorWeaponAndArmorEquipped_ShouldHaveCertainDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Aragorn");
            Weapon testAxe = new Weapon("Common Axe", 1, Weapon.WeaponTypes.AXE, new WeaponAttributes(7, 1.1));
            Armor testPlateBody = new Armor("Common place body armor", 1, Slots.BODY, ArmorTypes.PLATE, new PrimaryAttributes(1, 0, 0));
            double expected = (7 * 1.1) * (1 + ((5 + 1) * 0.01));

            // Act
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);
            double actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
