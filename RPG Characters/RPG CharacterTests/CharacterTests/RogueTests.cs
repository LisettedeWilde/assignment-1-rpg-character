﻿using RPG_Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPG_CharacterTests.CharacterTests
{
    public class RogueTests
    {
        /// <summary>
        /// Tests whether a level one character goes to level two when executing the method gainLevel()
        /// </summary>
        [Fact]
        public void GainLevel_LevelUpCharacter_ShouldBeLevelTwo()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 2;

            // Act
            rogue.GainLevel();
            int actual = rogue.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether a character is level one upon creation
        /// </summary>
        [Fact]
        public void Rogue_CreateCharacter_ShouldBeLevelOne()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 1;

            // Act
            int actual = rogue.Level;

            // Assert
            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Tests whether the character has the correct strength upon creation
        /// </summary>
        [Fact]
        public void Rogue_CreateCharacter_ShouldHaveCertainStrength()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 2;

            // Act
            int actual = rogue.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct dexterity upon creation
        /// </summary>
        [Fact]
        public void Rogue_CreateCharacter_ShouldHaveCertainDexterity()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 6;

            // Act
            int actual = rogue.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct intelligence upon creation
        /// </summary>
        [Fact]
        public void Rogue_CreateCharacter_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 1;

            // Act
            int actual = rogue.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct damage upon creation
        /// </summary>
        [Fact]
        public void Rogue_CreateCharacter_ShouldHaveCertainDamage()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            double expected = 1.06;

            // Act
            double actual = rogue.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct strength
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRogue_ShouldHaveCertainStrength()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 3;

            // Act
            rogue.GainLevel();
            int actual = rogue.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct dexterity
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRogue_ShouldHaveCertainDexterity()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 10;

            // Act
            rogue.GainLevel();
            int actual = rogue.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct intelligence
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRogue_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            int expected = 2;

            // Act
            rogue.GainLevel();
            int actual = rogue.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct damage
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRogue_ShouldHaveCertainDamage()
        {
            // Arrange
            Rogue rogue = new Rogue("Rogue");
            double expected = 1.1;

            // Act
            rogue.GainLevel();
            double actual = rogue.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
