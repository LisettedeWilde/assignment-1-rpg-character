﻿using RPG_Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPG_CharacterTests.CharacterTests
{
    public class RangerTests
    {
        /// <summary>
        /// Tests whether a level one character goes to level two when executing the method gainLevel()
        /// </summary>
        [Fact]
        public void GainLevel_LevelUpCharacter_ShouldBeLevelTwo()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 2;

            // Act
            ranger.GainLevel();
            int actual = ranger.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether a character is level one upon creation
        /// </summary>
        [Fact]
        public void Ranger_CreateCharacter_ShouldBeLevelOne()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 1;

            // Act
            int actual = ranger.Level;

            // Assert
            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Tests whether the character has the correct strength upon creation
        /// </summary>
        [Fact]
        public void Ranger_CreateCharacter_ShouldHaveCertainStrength()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 1;

            // Act
            int actual = ranger.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct dexterity upon creation
        /// </summary>
        [Fact]
        public void Ranger_CreateCharacter_ShouldHaveCertainDexterity()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 7;

            // Act
            int actual = ranger.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct intelligence upon creation
        /// </summary>
        [Fact]
        public void Ranger_CreateCharacter_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 1;

            // Act
            int actual = ranger.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct damage upon creation
        /// </summary>
        [Fact]
        public void Ranger_CreateCharacter_ShouldHaveCertainDamage()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            double expected = 1.07;

            // Act
            double actual = ranger.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct strength
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRanger_ShouldHaveCertainStrength()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 2;

            // Act
            ranger.GainLevel();
            int actual = ranger.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct dexterity
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRanger_ShouldHaveCertainDexterity()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 12;

            // Act
            ranger.GainLevel();
            int actual = ranger.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct intelligence
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRanger_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            int expected = 2;

            // Act
            ranger.GainLevel();
            int actual = ranger.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct damage
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoRanger_ShouldHaveCertainDamage()
        {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            double expected = 1.12;

            // Act
            ranger.GainLevel();
            double actual = ranger.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
