﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPG_Characters;

namespace RPG_CharacterTests.CharacterTests
{
    public class MageTests
    {
        /// <summary>
        /// Tests whether a level one character goes to level two when executing the method gainLevel()
        /// </summary>
        [Fact]
        public void GainLevel_LevelUpCharacter_ShouldBeLevelTwo()
        {
            // Arrange
            Mage mage = new Mage("Merlin");
            int expected = 2;

            // Act
            mage.GainLevel();
            int actual = mage.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether a character is level one upon creation
        /// </summary>
        [Fact]
        public void Mage_CreateCharacter_ShouldBeLevelOne()
        {
            // Arrange
            Mage mage = new Mage("Harry");
            int expected = 1;

            // Act
            int actual = mage.Level;

            // Assert
            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Tests whether the character has the correct strength upon creation
        /// </summary>
        [Fact]
        public void Mage_CreateCharacter_ShouldHaveCertainStrength()
        {
            // Arrange
            Mage mage = new Mage("Ron");
            int expected = 1;

            // Act
            int actual = mage.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct dexterity upon creation
        /// </summary>
        [Fact]
        public void Mage_CreateCharacter_ShouldHaveCertainDexterity()
        {
            // Arrange
            Mage mage = new Mage("Dumbledore");
            int expected = 1;

            // Act
            int actual = mage.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct intelligence upon creation
        /// </summary>
        [Fact]
        public void Mage_CreateCharacter_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Mage mage = new Mage("Hermione");
            int expected = 8;

            // Act
            int actual = mage.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct damage upon creation
        /// </summary>
        [Fact]
        public void Mage_CreateCharacter_ShouldHaveCertainDamage()
        {
            // Arrange
            Mage mage = new Mage("Malfoy");
            double expected = 1.08;

            // Act
            double actual = mage.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct strength
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoMage_ShouldHaveCertainStrength()
        {
            // Arrange
            Mage mage = new Mage("Ron");
            int expected = 2;

            // Act
            mage.GainLevel();
            int actual = mage.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct dexterity
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoMage_ShouldHaveCertainDexterity()
        {
            // Arrange
            Mage mage = new Mage("Dumbledore");
            int expected = 2;

            // Act
            mage.GainLevel();
            int actual = mage.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct intelligence
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoMage_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Mage mage = new Mage("Hermione");
            int expected = 13;

            // Act
            mage.GainLevel();
            int actual = mage.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct damage
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoMage_ShouldHaveCertainDamage()
        {
            // Arrange
            Mage mage = new Mage("Malfoy");
            double expected = 1.13;

            // Act
            mage.GainLevel();
            double actual = mage.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
