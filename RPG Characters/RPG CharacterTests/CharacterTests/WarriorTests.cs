using RPG_Characters;
using Xunit;

namespace RPG_CharacterTests.CharacterTests
{
    public class WarriorTests
    {
        /// <summary>
        /// Tests whether a level one character goes to level two when executing the method gainLevel()
        /// </summary>
        [Fact]
        public void GainLevel_LevelUpCharacter_ShouldBeLevelTwo()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 2;

            // Act
            warrior.GainLevel();
            int actual = warrior.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether a character is level one upon creation
        /// </summary>
        [Fact]
        public void Warrior_CreateCharacter_ShouldBeLevelOne()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 1;

            // Act
            int actual = warrior.Level;

            // Assert
            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Tests whether the character has the correct strength upon creation
        /// </summary>
        [Fact]
        public void Warrior_CreateCharacter_ShouldHaveCertainStrength()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 5;

            // Act
            int actual = warrior.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct dexterity upon creation
        /// </summary>
        [Fact]
        public void Warrior_CreateCharacter_ShouldHaveCertainDexterity()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 2;

            // Act
            int actual = warrior.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct intelligence upon creation
        /// </summary>
        [Fact]
        public void Warrior_CreateCharacter_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 1;

            // Act
            int actual = warrior.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the character has the correct damage upon creation
        /// </summary>
        [Fact]
        public void Warrior_CreateCharacter_ShouldHaveCertainDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior"); 
            double expected = 1.05;

            // Act
            double actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct strength
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoWarrior_ShouldHaveCertainStrength()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 8;

            // Act
            warrior.GainLevel();
            int actual = warrior.TotalPrimaryAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct dexterity
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoWarrior_ShouldHaveCertainDexterity()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 4;

            // Act
            warrior.GainLevel();
            int actual = warrior.TotalPrimaryAttributes.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct intelligence
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoWarrior_ShouldHaveCertainIntelligence()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            int expected = 2;

            // Act
            warrior.GainLevel();
            int actual = warrior.TotalPrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Tests whether the level two character has the correct damage
        /// </summary>
        [Fact]
        public void GainLevel_LevelTwoWarrior_ShouldHaveCertainDamage()
        {
            // Arrange
            Warrior warrior = new Warrior("Warrior");
            double expected = 1.08;

            // Act
            warrior.GainLevel();
            double actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
