﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Attributes
{
    public class WeaponAttributes
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }

        public WeaponAttributes(double damage, double attackSpeed)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
        }
    }
}
