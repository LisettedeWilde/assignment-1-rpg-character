﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

		public PrimaryAttributes(int strength, int dexterity, int intelligence)
		{
			Strength = strength;
			Dexterity = dexterity;
			Intelligence = intelligence;
		}
	}
}
