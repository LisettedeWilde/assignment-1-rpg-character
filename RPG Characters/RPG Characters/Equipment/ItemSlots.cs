﻿using System.Collections.Generic;

namespace RPG_Characters.Equipment
{
    public class ItemSlots
    {
        public Dictionary<Slots, Item> itemSlots;

        public enum Slots
        {
            HEAD,
            BODY,
            LEGS,
            WEAPON
        }

        public ItemSlots()
        {
            itemSlots = new Dictionary<Slots, Item>
            {
                { Slots.HEAD, new Armor() },
                { Slots.BODY, new Armor() },
                { Slots.LEGS, new Armor() },
                { Slots.WEAPON, new Weapon() }
            };
        }

        /// <summary>
        /// Adds weapon to the weapon slot
        /// </summary>
        /// <param name="weapon"></param>
        public void AddWeapon(Weapon weapon)
        {
            this.itemSlots[weapon.Slot] = weapon;
            System.Console.WriteLine("New weapon equipped!");
        }

        /// <summary>
        /// Adds armor to the slot the item is equipped 
        /// </summary>
        /// <param name="armor"></param>
        public void AddArmor(Armor armor)
        {
            this.itemSlots[armor.Slot] = armor;
            System.Console.WriteLine("New armor equipped!");
        }
    }
}
