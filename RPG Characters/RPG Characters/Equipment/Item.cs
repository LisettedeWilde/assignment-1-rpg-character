﻿namespace RPG_Characters.Equipment
{
    public abstract class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public ItemSlots.Slots Slot { get; set; }

        public Item() { }

        public Item(string name, int requiredLevel)
        {
            Name = name;
            RequiredLevel = requiredLevel;
        }
    }
}
