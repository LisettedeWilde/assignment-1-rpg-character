﻿namespace RPG_Characters.Equipment
{
    public class Armor : Item
    {
        internal PrimaryAttributes PrimaryAttributes { get; set; }
        internal ArmorTypes ArmorType { get; set; }

        public enum ArmorTypes
        {
            NULL,
            CLOTH,
            LEATHER,
            MAIL,
            PLATE
        }

        public Armor() 
        {
            this.ArmorType = ArmorTypes.NULL;
            this.RequiredLevel = 0;
            this.PrimaryAttributes = new PrimaryAttributes(0, 0, 0);
        }

        public Armor(string name, int requiredLevel, ItemSlots.Slots slot, ArmorTypes armorType, PrimaryAttributes primaryAttributes) : base(name, requiredLevel)
        {
            this.Slot = slot;
            this.ArmorType = armorType;
            this.PrimaryAttributes = primaryAttributes;
        }
    }
}
