﻿using RPG_Characters.Attributes;
using System;

namespace RPG_Characters.Equipment
{
    public class Weapon : Item
    {
        public WeaponAttributes WeaponAttributes { get; set; }
        internal WeaponTypes WeaponType { get; set; }

        public enum WeaponTypes
        {
            AXE,
            BOW,
            DAGGER,
            HAMMER,
            STAFF,
            SWORD,
            WAND,
            NULL
        }

        public Weapon() 
        {
            this.WeaponType = WeaponTypes.NULL;
            this.RequiredLevel = 0;
            WeaponAttributes = new WeaponAttributes(0, 0);
        }

        public Weapon(string name, int requiredLevel, WeaponTypes weaponType, WeaponAttributes weaponAttributes) : base(name, requiredLevel)
        {
            this.Slot = ItemSlots.Slots.WEAPON;
            this.WeaponType = weaponType;
            WeaponAttributes = weaponAttributes;
        }
    }
}
