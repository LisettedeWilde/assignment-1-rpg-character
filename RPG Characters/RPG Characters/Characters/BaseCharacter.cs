﻿using RPG_Characters.Equipment;
using RPG_Characters.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using static RPG_Characters.Equipment.Armor;
using static RPG_Characters.Equipment.Weapon;

namespace RPG_Characters
{
    public abstract class BaseCharacter
    {
        public string Name { get; set; }
        public string Character { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public double Damage { get; set; }
        protected List<WeaponTypes> AllowedWeapons { get; set; }
        public List<ArmorTypes> AllowedArmor { get; set; }

        protected PrimaryAttributes basePrimaryAttributes;
		protected ItemSlots equipment;
        protected int lvlStrengthGain;
        protected int lvlDexterityGain;
        protected int lvlIntelligenceGain;
        protected bool weaponEquipped;

        public BaseCharacter(string name)
		{
			Name = name;
			Level = 1;
			equipment = new ItemSlots();
            weaponEquipped = false;
            AllowedWeapons = new List<WeaponTypes>();
            AllowedArmor = new List<ArmorTypes>();
		}

        /// <summary>
        /// Writes the character stats (name, level, strength, dexterity, intelligence, damage) to the console
        /// </summary>
        public void ReturnCharacterStats()
        {
			StringBuilder s = new StringBuilder();

			s.AppendLine("Character Information");
			s.AppendLine("---------------------");
			s.AppendLine("Name : " + this.Name);
			s.AppendLine("Level :        " + this.Level);
            s.AppendLine("Strength :     " + this.TotalPrimaryAttributes.Strength);
            s.AppendLine("Dexterity :    " + this.TotalPrimaryAttributes.Dexterity);
            s.AppendLine("Intelligence : " + this.TotalPrimaryAttributes.Intelligence);
			s.AppendLine("Damage :       " + this.Damage);

            Console.WriteLine(s);
        }

        /// <summary>
        /// Calculates the total primary attributes, based on the 
        /// character's level and the armor the character has equipped.
        /// </summary>
		public void CalculateTotalPrimaryAttributes()
        {
            int levelMinusOne = this.Level - 1;

            int armorStrength = 0;
            int armorDexterity = 0;
            int armorIntelligence = 0;

            // Loop through the equipment slots
            foreach (ItemSlots.Slots slot in ItemSlots.Slots.GetValues(typeof(ItemSlots.Slots)))
            {
                // skip the weapon slot as it doesn't contain primaryattributes
                if (slot == ItemSlots.Slots.WEAPON)
                    continue;
                // Add the primary attribute values from the equipped armor to the armorStrength,
                // armorDexterity and armorIntelligence variables
                else
                {
                    armorStrength += ((Armor)this.equipment.itemSlots[slot]).PrimaryAttributes.Strength;
                    armorDexterity += ((Armor)this.equipment.itemSlots[slot]).PrimaryAttributes.Dexterity;
                    armorIntelligence += ((Armor)this.equipment.itemSlots[slot]).PrimaryAttributes.Intelligence;
                }
            }
            // calculate final value of attributes based on the sum:
            // total attribute = attributes from level + atributes from all equipped armor
            int totalStrength = basePrimaryAttributes.Strength + this.lvlStrengthGain * levelMinusOne + armorStrength;
            int totalDexterity = basePrimaryAttributes.Dexterity + this.lvlDexterityGain * levelMinusOne + armorDexterity;
            int totalIntelligence = basePrimaryAttributes.Intelligence + this.lvlIntelligenceGain * levelMinusOne + armorIntelligence;

            // update total primary attributes
            this.TotalPrimaryAttributes = new PrimaryAttributes(totalStrength, totalDexterity, totalIntelligence);
            // Update damage based on changed totalprimaryattributes
            calculateDamage();
        }

        /// <summary>
        /// Increases the level of the character by 1
        /// </summary>
        public void GainLevel()
        {
            this.Level += 1;
            CalculateTotalPrimaryAttributes();
            calculateDamage();
        }

        /// <summary>
        /// Calculates the total damage based on the damage per second of 
        /// the weapon and the relevant primary attribute for the character
        /// </summary>
        public abstract void calculateDamage();

        /// <summary>
        /// Adds weapon to the weapon slot in equipment, and checks whether the weapon can be equipped by the character
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="InvalidWeaponException">Checks whether the weapon is valid based on its level and if the character can equip this weapon</exception>
        public void EquipWeapon(Weapon weapon)
        {
            // check if weapon level is lower or equal to character level
            if (weapon.RequiredLevel <= this.Level)
            {
                // check if weapon type is compatible with character type
                if (AllowedWeapons.Contains(weapon.WeaponType))
                {
                    equipment.AddWeapon(weapon);
                    weaponEquipped = true;
                    // update damage based on new weapon attributes
                    calculateDamage();
                }
                else
                    throw new InvalidWeaponException(weapon.WeaponType, this.Character);
            }
            else
            {
                throw new InvalidWeaponException(weapon.RequiredLevel, this.Level);
            }
        }

        /// <summary>
        /// Adds armor to a specific armor slot in equipment, and checks whether the armor can be equipped by the character
        /// </summary>
        /// <param name="armor"></param>
        /// <exception cref="InvalidArmorException">Checks whether the armor is valid based on its level and if the character can equip this armor</exception>
        public void EquipArmor(Armor armor)
        {
            // check if armor level is lower or equal to character level
            if (armor.RequiredLevel <= this.Level)
            {
                // check if armor type is compatible with character type
                if (AllowedArmor.Contains(armor.ArmorType))
                {
                    equipment.AddArmor(armor);
                    // update total primary attributes based on new armor attributes
                    CalculateTotalPrimaryAttributes();
                }
                else
                    throw new InvalidArmorException(armor.ArmorType, this.Character);
            }
            else
                throw new InvalidArmorException(armor.RequiredLevel, this.Level);
        }
    }
}
