﻿using RPG_Characters.Equipment;
using RPG_Characters.Exceptions;
using System;
using static RPG_Characters.Equipment.Armor;
using static RPG_Characters.Equipment.Weapon;

namespace RPG_Characters
{
    public class Warrior : BaseCharacter
    {
		public Warrior(string name) : base(name)
		{
			this.basePrimaryAttributes = new PrimaryAttributes(5, 2, 1);
            CalculateTotalPrimaryAttributes();
            calculateDamage();
            this.Character = "warrior";
            this.lvlStrengthGain = 3;
            this.lvlDexterityGain = 2;
            this.lvlIntelligenceGain = 1;

            AllowedWeapons.Add(WeaponTypes.AXE);
            AllowedWeapons.Add(WeaponTypes.HAMMER);
            AllowedWeapons.Add(WeaponTypes.SWORD);
            AllowedArmor.Add(ArmorTypes.MAIL);
            AllowedArmor.Add(ArmorTypes.PLATE);
        }

        public override void calculateDamage()
        {
            double DPS = 0;
            if (!weaponEquipped)
                DPS = 1;
            else
            {
                // calculate damage per second: damage * attack speed
                DPS = ((Weapon)this.equipment.itemSlots[ItemSlots.Slots.WEAPON]).WeaponAttributes.Damage * ((Weapon)this.equipment.itemSlots[ItemSlots.Slots.WEAPON]).WeaponAttributes.AttackSpeed;
            }
            
            // calculate damage based on the calculation:
            // character damage = weapon DPS * (1 + totalPrimaryAttributes / 100)
            this.Damage = (double)DPS * (1 + this.TotalPrimaryAttributes.Strength * 0.01);
        }
    }
}
