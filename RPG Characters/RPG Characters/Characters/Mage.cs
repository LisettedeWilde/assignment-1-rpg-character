﻿using RPG_Characters.Equipment;
using RPG_Characters.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPG_Characters.Equipment.Armor;
using static RPG_Characters.Equipment.Weapon;

namespace RPG_Characters
{
    public class Mage : BaseCharacter
    {
        public Mage(string name) : base(name)
        {
            this.basePrimaryAttributes = new PrimaryAttributes(1, 1, 8);
            this.Character = "Mage";
            CalculateTotalPrimaryAttributes();
            calculateDamage();
            this.lvlStrengthGain = 1;
            this.lvlDexterityGain = 1;
            this.lvlIntelligenceGain = 5;

            AllowedWeapons.Add(WeaponTypes.STAFF);
            AllowedWeapons.Add(WeaponTypes.WAND);
            AllowedArmor.Add(ArmorTypes.CLOTH);
        }

        public override void calculateDamage()
        {
            double DPS = 0;
            if (!weaponEquipped)
                DPS = 1;
            else
            {
                // calculate damage per second: damage * attack speed
                DPS = ((Weapon)this.equipment.itemSlots[ItemSlots.Slots.WEAPON]).WeaponAttributes.Damage * ((Weapon)this.equipment.itemSlots[ItemSlots.Slots.WEAPON]).WeaponAttributes.AttackSpeed;
            }
            // calculate damage based on the calculation:
            // character damage = weapon DPS * (1 + totalPrimaryAttributes / 100)
            this.Damage = Math.Round((double)DPS * (1 + this.TotalPrimaryAttributes.Intelligence * 0.01), 2);
        }
    }
}
