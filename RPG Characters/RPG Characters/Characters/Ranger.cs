﻿using RPG_Characters.Equipment;
using RPG_Characters.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPG_Characters.Equipment.Armor;
using static RPG_Characters.Equipment.Weapon;

namespace RPG_Characters
{
    public class Ranger : BaseCharacter
    {
        public Ranger(string name) : base(name)
        {
            this.basePrimaryAttributes = new PrimaryAttributes(1, 7, 1);
            CalculateTotalPrimaryAttributes();
            calculateDamage();
            this.Character = "Ranger";
            this.lvlStrengthGain = 1;
            this.lvlDexterityGain = 5;
            this.lvlIntelligenceGain = 1;

            AllowedWeapons.Add(WeaponTypes.BOW);
            AllowedArmor.Add(ArmorTypes.MAIL);
            AllowedArmor.Add(ArmorTypes.LEATHER);
        }

        public override void calculateDamage()
        {
            double DPS = 0;
            if (!weaponEquipped)
                DPS = 1;
            else
            {
                // calculate damage per second: damage * attack speed
                DPS = ((Weapon)this.equipment.itemSlots[ItemSlots.Slots.WEAPON]).WeaponAttributes.Damage * ((Weapon)this.equipment.itemSlots[ItemSlots.Slots.WEAPON]).WeaponAttributes.AttackSpeed;
            }
            // calculate damage based on the calculation:
            // character damage = weapon DPS * (1 + totalPrimaryAttributes / 100)
            this.Damage = Math.Round((double)DPS * (1 + this.TotalPrimaryAttributes.Dexterity * 0.01), 2);
        }
    }
}
