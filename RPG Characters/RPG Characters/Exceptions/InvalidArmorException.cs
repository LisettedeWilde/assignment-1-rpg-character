﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPG_Characters.Equipment.Armor;

namespace RPG_Characters.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() { }

        /// <summary>
        /// Checks whether the armortype can be equipped by a specific character
        /// </summary>
        /// <param name="armorName"></param>
        /// <param name="characterName"></param>
        public InvalidArmorException(ArmorTypes armorName, string characterName)
            : base(String.Format("Invalid armor type: {0} for character type: {1}", armorName, characterName))
        {

        }

        /// <summary>
        /// Checks whether the armor can be equipped by a character based on its level
        /// </summary>
        /// <param name="armorLevel"></param>
        /// <param name="characterLevel"></param>
        public InvalidArmorException(int armorLevel, int characterLevel)
            : base(String.Format("Invalid armor level: {0} for character level: {1}", armorLevel, characterLevel))
        {

        }
    }
}
