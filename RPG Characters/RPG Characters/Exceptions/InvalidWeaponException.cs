﻿using RPG_Characters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPG_Characters.Equipment.Weapon;

namespace RPG_Characters.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() { }

        public InvalidWeaponException(WeaponTypes weaponName, string characterName)
            : base(String.Format("Invalid weapon type: {0} for character type: {1}", weaponName, characterName))
        {

        }

        public InvalidWeaponException(int weaponLevel, int characterLevel)
            : base(String.Format("Invalid weapon level: {0} for character level: {1}", weaponLevel, characterLevel))
        {

        }
    }
}
