﻿using RPG_Characters.Attributes;
using RPG_Characters.Equipment;
using System;
using static RPG_Characters.Equipment.Armor;
using static RPG_Characters.Equipment.ItemSlots;
using static RPG_Characters.Equipment.Weapon;

namespace RPG_Characters
{
    class Program
    {
        static void Main(string[] args)
        {
            Mage mage = new Mage("Merlin");
            Weapon staff = new Weapon("Awesome staff", 1, WeaponTypes.STAFF, new WeaponAttributes(6, 4));
            mage.EquipWeapon(staff);
            Armor cloth = new("Indestructable cloth", 1, Slots.BODY, ArmorTypes.CLOTH, new PrimaryAttributes(1, 2, 3));
            mage.EquipArmor(cloth);
            mage.GainLevel();

            mage.ReturnCharacterStats();

            Warrior warrior = new Warrior("Arthur");
            Weapon hammer = new Weapon("Awesome Hammer", 1, WeaponTypes.HAMMER, new WeaponAttributes(3, 9));
            warrior.EquipWeapon(hammer);
            Armor plate = new("Indestructable Plate", 1, Slots.BODY, ArmorTypes.PLATE, new PrimaryAttributes(3, 4, 2));
            warrior.EquipArmor(plate);
            warrior.GainLevel();

            warrior.ReturnCharacterStats();

            Rogue rogue = new Rogue("Gimli");
            Weapon sword = new Weapon("Common sword", 1, WeaponTypes.SWORD, new WeaponAttributes(2, 8));
            rogue.EquipWeapon(sword);
            Armor mail = new("Mail", 1, Slots.HEAD, ArmorTypes.MAIL, new PrimaryAttributes(3, 6, 1));
            rogue.EquipArmor(mail);
            rogue.GainLevel();

            rogue.ReturnCharacterStats();

            Ranger ranger = new Ranger("Legolas");
            Weapon bow = new Weapon("Bow", 1, WeaponTypes.BOW, new WeaponAttributes(10, 2));
            ranger.EquipWeapon(bow);
            Armor leather = new Armor("leather", 1, Slots.LEGS, ArmorTypes.LEATHER, new PrimaryAttributes(5, 9, 3));
            ranger.EquipArmor(leather);
            ranger.GainLevel();

            ranger.ReturnCharacterStats();

            Console.ReadLine();
        }
    }
}
